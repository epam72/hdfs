FROM ubuntu:latest

ARG HADOOP_VERSION=3.3.0
ARG USER=hadoop
ARG GROUP=hadoop

ARG HADOOP_DOWNLOAD_URL=https://downloads.apache.org/hadoop/common/hadoop-${HADOOP_VERSION}/hadoop-${HADOOP_VERSION}.tar.gz

RUN mkdir -p /usr/share/man/man1 && \
	apt -qq update && \
	apt -qq install -y \
		dumb-init \
		openjdk-8-jre-headless \
		xmlstarlet \
		nano \
		wget \
		curl \
		jq

RUN groupadd ${GROUP} && useradd --home-dir /opt/hadoop --gid ${GROUP} --no-create-home --shell /bin/bash ${USER}

RUN cd /opt/
RUN wget -q ${HADOOP_DOWNLOAD_URL}
RUN tar -zxf hadoop-${HADOOP_VERSION}.tar.gz --directory /opt
RUN rm hadoop-${HADOOP_VERSION}.tar.gz

RUN cd /opt \
	&& ln -s ./hadoop-${HADOOP_VERSION} ./hadoop \
	&& mkdir -p -m 755 /var/log/hadoop \
	&& chown ${USER}:${GROUP} /var/log/hadoop

COPY ./entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh", "hdfs"]

RUN for i in $(seq 1 8); do mkdir -p -m 750 /data${i} && chown ${USER}:${GROUP} /data${i}; done
VOLUME /data1 /data2 /data3 /data4 /data5 /data6 /data7 /data8

USER ${USER}
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/jre/
ENV HADOOP_HOME /opt/hadoop
ENV HADOOP_LOG_DIR /var/log/hadoop
ENV PATH $HADOOP_HOME/bin:$PATH

RUN hadoop checknative -a || true
